Picasa Albums
=================

Drupal module (Drupal 6.x) for attaching Picasa albums to nodes. 
It was originally based on Picasa Node Album but then further 
modified and improved.

Picasa Albums Features
----------------------
- Access all albums (private, public, link) from one Picasa user
- Supports Zend Framework 2 (ZF2), installed with Zend module
- Contains an album overview page, displaying all the albums you added
- Allows to specify thumbnail size, image size
- Albums are passed to every node, simplifies lifes of template designers

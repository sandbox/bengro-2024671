<?php
/**
 * @file
 * Enables the association of picasa galleries to nodes.
 */

/**
 * Implements hook_init().
 */
function picasa_albums_init() {
  // TODO: Only embed css when necessary.
  drupal_add_css(drupal_get_path('module', 'picasa_albums') . '/picasa-albums.css');
}

/**
 * Implements hook_perm().
 */
function picasa_albums_perm() {
  return array('view albums', 'add albums', 'remove albums');
}

/**
 * Implements hook_theme().
 */
function picasa_albums_theme() {
  return array(
    'picasa_albums_list' => array(
      'arguments' => array('nid' => 0, 'albums' => array()),
      'template' => 'picasa-albums-list',
    ),
    'picasa_albums_detail' => array(
      'arguments' => array(
        'nid' => 0,
        'node_url' => '',
        'title' => '',
        'uid' => 0,
        'author' => '',
        'postedby' => '',
        'images' => array(),),
      'template' => 'picasa-albums-detail',
    ),
    'picasa_albums_overview' => array(
      'arguments' => array('albums' => array()),
      'template' => 'picasa-albums-overview',
    ),
    'picasa_albums_list_embedded' => array(
      'arguments' => array('nid' => 0, 'albums' => array()),
      'template' => 'picasa-albums-list-embedded',
    ),
  );
}

/**
 * Implements hook_menu().
 */
function picasa_albums_menu() {

  $items = array();

  $items['js/picasa_albums'] = array(
    'page callback'   => 'picasa_albums_js_albums',
    'access callback' => TRUE,
    'type'            => MENU_CALLBACK,
    'file'            => 'picasa_albums.pages.inc',
  );

  // TODO: check if Picasa Albums is activated for node type.
  $items['node/%/manage-albums'] = array(
    'title'             => 'Manage Albums',
    'description'       => 'Configuration options for Picasa Albums',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('picasa_albums_add', 1),
    'access callback'   => 'user_access',
    'access arguments'  => array('add albums'),
    'type'              => MENU_LOCAL_TASK,
    'file'              => 'picasa_albums.pages.inc',
  );

  $menu_type = variable_get('picasa_albums_view_tab', 0) ? MENU_LOCAL_TASK : MENU_CALLBACK;
  $items['node/%/albums'] = array(
    'title'             => 'Albums',
    'description'       => 'Individual album view',
    'page callback'     => 'picasa_albums_detail',
    'page arguments'    => array(1, 3),
    'access callback'   => '_picasa_albums_show_tab',
    'access arguments'  => array(1),
    'type'              => $menu_type,
    'file'              => 'picasa_albums.pages.inc',
  );

  $items['admin/settings/picasa_albums'] = array(
    'title' => 'Picasa Albums',
    'description' => 'Allows the user to configure the lightbox2 settings',
    'file' => 'picasa_albums.admin-main.inc',
    'page callback' => 'picasa_albums_admin_main',
    'access arguments' => array('administer site configuration'),
    'access callback'   => 'user_access',
  );

  $items['admin/settings/picasa_albums/general'] = array(
    'title'             => 'Overview',
    'description'       => 'Configuration options for Picasa Albums',
    'page callback'     => 'picasa_albums_admin_main',
    'access arguments' => array('administer site configuration'),
    'access callback'   => 'user_access',
    'type'              => MENU_DEFAULT_LOCAL_TASK,
    'file'              => 'picasa_albums.admin-main.inc',
  );

  $items['admin/settings/picasa_albums/accounts'] = array(
    'title'             => 'Picasa Account',
    'description'       => 'Google Account login credentials.',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('picasa_albums_admin_accounts'),
    'access arguments'  => array('administer site configuration'),
    'access callback'   => 'user_access',
    'type'              => MENU_LOCAL_TASK,
    'file'              => 'picasa_albums.admin-accounts.inc',
    'weight'            => 1,
  );

  $items['admin/settings/picasa_albums/presentation'] = array(
    'title'             => 'Presentation',
    'description'       => 'Presentation related options for Picasa Albums',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('picasa_albums_admin_presentation'),
    'access arguments'  => array('administer site configuration'),
    'access callback'   => 'user_access',
    'type'              => MENU_LOCAL_TASK,
    'file'              => 'picasa_albums.admin-presentation.inc',
    'weight'            => 2,
  );

  $items['admin/settings/picasa_albums/options'] = array(
    'title'             => 'Options',
    'description'       => 'Options related related to Picasa Albums module',
    'page callback'     => 'drupal_get_form',
    'page arguments'    => array('picasa_albums_admin_options'),
    'access arguments'  => array('administer site configuration'),
    'access callback'   => 'user_access',
    'type'              => MENU_LOCAL_TASK,
    'file'              => 'picasa_albums.admin-options.inc',
    'weight'            => 3,
  );

  if (variable_get('picasa_albums_overview_activated', '0') != 0) {
    $items['albums'] = array(
      'title' => 'Albums Overview',
      'description' => 'Albums stores in the database.',
      'type'              => MENU_CALLBACK,
      'file'              => 'picasa_albums.pages.inc',
      'page callback'     => 'picasa_albums_overview',
      'access arguments' => array('view albums'),
    );
  }

  return $items;
}


/**
 * Custom access loader to handle node types as well as access.
 */
function _picasa_albums_show_tab($nid) {
  if (!user_access('view albums')) {
    return FALSE;
  }
  // Now check that this node type should have picasa albums.
  $node = node_load($nid);
  $types = _picasa_albums_get_node_types();
  if (!$types[$node->type]) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Implements hook_nodeapi().
 */
function picasa_albums_nodeapi(&$node, $op, $a3 = NULL, $a4 = NULL) {

  // Load all albums for this node.
  $albums = _picasa_albums_get_list($node->nid);

  // Set theme variable if users wishes to do so.
  if (variable_get('picasa_albums_variable', '0') != 0) {
    $node->picasa_albums = array(
      '#subject' => t('Albums for node'),
      '#value' => theme('picasa_albums_list_embedded', $node->nid, $albums),
      '#albums' => $albums,
      '#weight' => variable_get('picasa_albums_view_weight', 10),
    );
  }

  // Load all node types.
  $types = _picasa_albums_get_node_types();
  if (!$types[$node->type] || $op != 'view' || !user_access('view picasa albums')) {
    return;
  }

  // Return if this is a teaser and we're not set to display.
  if ($a3 && !variable_get('picasa_albums_view_teaser', FALSE)) {
    return;
  }

  // Return if this is a page and we're not set to display.
  if ($a4 && !variable_get('picasa_albums_view_page', TRUE)) {
    return;
  }

  $node->content['picasa_albums_list'] = array(
    '#subject' => t('Albums'),
    '#value' => theme('picasa_albums_list', $node->nid, $albums),
    '#weight' => variable_get('picasa_albums_view_weight', 10),
  );

}

/**
 * Returns an array of albums for he node given.
 */
function _picasa_albums_get_list($nid) {

  // Load up albums.
  $result = db_query("SELECT uid, google_user, album_id, title, added, updated, images FROM {picasa_albums} WHERE nid = %d", $nid);
  $albums = array();
  $cache_min = variable_get('picasa_albums_cache_min', 60);
  $preview_images = variable_get('picasa_albums_preview_num', 0);

  while ($row = db_fetch_object($result)) {

    $account = user_load($row->uid);
    $albums[$row->album_id]['postedby'] = t('posted by') . ' ' . l($account->name, 'user/' . $account->uid, array('html' => TRUE));
    $albums[$row->album_id]['author'] = l($account->name, 'user/' . $account->uid, array('html' => TRUE));
    $albums[$row->album_id]['title'] = $row->title;
    $albums[$row->album_id]['uid'] = $account->uid;
    $albums[$row->album_id]['link'] = l(t('View Entire Album'), 'node/' . $nid . '/albums/' . $row->album_id);

    $cache_expires = $row->updated + $cache_min;
    if (empty($row->images) || time() > $cache_expires) {
      $albums[$row->album_id]['images'] = _picasa_albums_get_images($row->google_user, $row->album_id);
    }
    else {
      $albums[$row->album_id]['images'] = unserialize($row->images);
    }

    $albums[$row->album_id]['image_count'] = count($albums[$row->album_id]['images']);
    $albums[$row->album_id]['total_images'] = format_plural(count($albums[$row->album_id]['images']), '1 image', '@count images');

    // Set preview array for images.
    if ($preview_images > 0 && $preview_images < count($albums[$row->album_id]['images'])) {
      $albums[$row->album_id]['images'] = array_slice($albums[$row->album_id]['images'], 0, $preview_images);
      $albums[$row->album_id]['preview_mode'] = TRUE;
    }

  }

  return $albums;
}

/**
 * Custom access loader to handle node types as well as access.
 */
function _picasa_albums_access_loader($nid) {
  if (!user_access('add picasa albums')) {
    return FALSE;
  }
  // Now check that this node type should have picasa albums.
  $node = node_load($nid);
  $types = _picasa_albums_get_node_types();
  if (!$types[$node->type]) {
    return FALSE;
  }

  return TRUE;
}

/**
 * This function logs in and returns the google client.
 */
function _picasa_albums_get_client() {

  // Improve: This is not safe and does not support multiple accounts.
  $user = variable_get('picasa_albums_google_user', '');
  $pass = variable_get('picasa_albums_google_pass', '');
  $service = ZendGData\Photos::AUTH_SERVICE_NAME;

  if ($user != '') {

    try {
      // This is used to circumvent potential SSL problems at the hoster, see:
      // http://goo.gl/a6Eof.
      $adapter = new \Zend\Http\Client\Adapter\Curl();
      $http_client = new \ZendGData\HttpClient();
      $http_client->setAdapter($adapter);
      $client = ZendGData\ClientLogin::getHttpClient($user, $pass, $service, $http_client);
    }
    catch (ZendGData\App\HttpException $e) {
      if ($e->getResponse() != NULL) {
        watchdog('Picasa Albums', $e->getResponse()->getBody(), array(), WATCHDOG_ERROR);
        return FALSE;
      }
    }
    catch (\ZendGData\App\Exception $e) {
      watchdog('Picasa Albums', $e->getMessage(), array(), WATCHDOG_ERROR);
      return FALSE;
    }
  }

  return $client;
}

/**
 * This function returns all albums of a Picasa user.
 */
function _picasa_albums_get_albums($key = 'id') {

  global $user;

  $google_user = variable_get('picasa_albums_google_user', '');
  $client = _picasa_albums_get_client();

  $gp = new \ZendGData\Photos($client, "Google-DevelopersGuide-1.0");
  $albums = array();

  try {
    $albums_query = new \ZendGData\Photos\UserQuery();
    $albums_query->setUser($google_user);
    $albums_query->setAccess('all');

    $user_feed = $gp->getUserFeed($google_user, $albums_query);
    foreach ($user_feed as $user_entry) {
      if ($key == 'id') {
        $albums[$user_entry->gphotoId->text] = $user_entry->title->text;
      }
      else {
        $albums[$user_entry->title->text] = $user_entry->title->text;
      }
    }
  }
  catch (ZendGData\App\HttpException $e) {
    watchdog('Picasa Albums', $e->getMessage(), array(), WATCHDOG_ERROR);
    if ($e->getResponse() != NULL) {
      watchdog('Picasa Albums', $e->getResponse()->getBody(), array(), WATCHDOG_ERROR);
    }
  }
  catch (\ZendGData\App\Exception $e) {
    watchdog('Picasa Albums', $e->getMessage(), array(), WATCHDOG_ERROR);
  }

  return $albums;
}

/**
 * This function returns an array of images for the given album id.
 */
function _picasa_albums_get_images($google_user, $album_id) {
  $client = _picasa_albums_get_client();

  $gp = new ZendGData\Photos($client, "Google-DevelopersGuide-1.0");

  try {
    $query = new ZendGData\Photos\AlbumQuery($album_id);

    // $query->setUser($google_user);
    $query->setAlbumId($album_id);
    $query->setThumbsize(variable_get('picasa_albums_thumbnail_size', '104c'));
    $query->setImgMax(variable_get('picasa_albums_img_size', '800u'));
    $album_feed = $gp->getAlbumFeed($query);

    foreach ($album_feed as $album_entry) {
      $images[] = _picasa_albums_get_image_data($album_entry);
    }

    // Update node and cache.
    db_query("UPDATE {picasa_albums} SET updated = %d, images = '%s' WHERE google_user = '%s' AND album_id = '%s'", time(), serialize($images), $google_user, $album_id);

  }
  catch (ZendGData\App\HttpException $e) {
    watchdog('Picasa Albums', $e->getMessage(), array(), WATCHDOG_ERROR);
    if ($e->getResponse() != NULL) {
      watchdog('Picasa Albums', $e->getResponse()->getBody(), array(), WATCHDOG_ERROR);
    }
  }
  catch (\ZendGData\App\Exception $e) {
    watchdog('Picasa Albums', $e->getMessage(), array(), WATCHDOG_ERROR);
  }

  return $images;
}

/**
 * This function retuns a formatted array of image properties.
 */
function _picasa_albums_get_image_data($photo_entry) {
  $camera = "";
  $content_url = "";
  $first_thumbnail_url = "";
  $title = "";
  $summary = "";

  $album_id = $photo_entry->getGphotoAlbumId()->getText();
  $photo_id = $photo_entry->getGphotoId()->getText();
  $title = $photo_entry->getTitle()->getText();
  $summary = $photo_entry->getSummary()->getText();

  if ($photo_entry->getExifTags() != NULL &&
    $photo_entry->getExifTags()->getMake() != NULL &&
    $photo_entry->getExifTags()->getModel() != NULL) {
    $camera = $photo_entry->getExifTags()->getMake()->getText() . " " . $photo_entry->getExifTags()->getModel()->getText();
  }

  if ($photo_entry->getMediaGroup()->getContent() != NULL) {
    $media_content_array = $photo_entry->getMediaGroup()->getContent();
    $content_url = $media_content_array[0]->getUrl();
  }

  if ($photo_entry->getMediaGroup()->getThumbnail() != NULL) {
    $media_thumbnail_array = $photo_entry->getMediaGroup()->getThumbnail();
    $first_thumbnail_url = $media_thumbnail_array[0]->getUrl();
  }

  $image = array();
  $image['album_id'] = $album_id;
  $image['photo_id'] = $photo_id;
  $image['camera'] = $camera;
  $image['image'] = $content_url;
  $image['thumbnail'] = $first_thumbnail_url;
  $image['title'] = $title;
  $image['summary'] = $summary;

  return $image;
}


/**
 * This function returns the selected node types for picasa albums.
 */
function _picasa_albums_get_node_types() {
  $node_types = unserialize(filter_xss(variable_get('picasa_albums_node_types', serialize(array()))));
  return $node_types;
}


/**
 * Updates all albums in database.
 */
function picasa_albums_update() {

  $result = db_query("SELECT uid, google_user, album_id, title, added, updated, images FROM {picasa_albums}");
  $albums = array();

  while ($row = db_fetch_object($result)) {
    _picasa_albums_get_images($row->google_user, $row->album_id);
  }

}

<?php

/**
 * @file
 * Admin page callbacks for the picasa_albums module
 */

/**
 * Options page.
 */
function picasa_albums_admin_options() {

  if (!$client = _picasa_albums_get_client()) {
    drupal_set_message(t('There was a problem with logging you into Picasa. Please make sure you enter your Google username and password.'), 'error');
  }

  $form['cache'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cache Settings'),
  );

  $options = array(
    '0' => t('<none>'),
    '60' => t('1 min'),
    '180' => t('3 min'),
    '300' => t('5 min'),
    '600' => t('10 min'),
    '900' => t('15 min'),
    '1800' => t('30 min'),
    '2700' => t('45 min'),
    '3600' => t('1 hour'),
    '10800' => t('3 hours'),
    '21600' => t('6 hours'),
    '32400' => t('9 hours'),
    '43200' => t('12 hours'),
    '86400' => t('1 day'),
    '172800' => t('2 days'),
    '259200' => t('3 days'),
    '604800' => t('7 days'),
    '1209600' => t('14 days'),
    '2419200' => t('1 Month'),
  );
  $form['cache']['picasa_albums_cache_min'] = array(
    '#type' => 'select',
    '#title' => t('Minimum cache lifetime'),
    '#description' => t('Your server needs to contact Google every time it loads fresh album data. If you set a cache lifetime of 1 hour, then your server will store the results locally and only contact Google every hour.'),
    '#options' => $options,
    '#default_value' => variable_get('picasa_albums_cache_min', 21600),
  );

  $form['cache']['picasa_albums_reset_albums'] = array(
    '#type' => 'checkbox',
    '#title' => t('Update all albums'),
    '#description' => t('This option updates all the updates of all accounts.'),
    '#default_value' => 0,
    '#return_value' => 1,
  );

  $form['sizes'] = array(
    '#type' => 'fieldset',
    '#title' => t('Image sizes'),
  );

  $form['sizes']['picasa_albums_thumbnail_size'] = array(
    '#title' => t('Thumbnail size'),
    '#description' => t('Possible Values: 32, 48, 64, 72, 104, 144, 150, 160. Append a u for uncropped and c for cropped. Example: 72c'),
    '#type' => 'textfield',
    '#cols' => 5,
    '#default_value' => variable_get('picasa_albums_thumbnail_size', '104c'),
  );

  $form['sizes']['picasa_albums_img_size'] = array(
    '#title' => t('Image Size'),
    '#description' => t('Possible values 4, 110, 128, 200, 220, 288, 320, 400, 512, 576, 640, 720, 800, 912, 1024, 1152, 1280, 1440, 1600. Append u for uncropped, and c for cropped. Example: 110c.'),
    '#type' => 'textfield',
    '#cols' => 5,
    '#default_value' => variable_get('picasa_albums_img_size', '800u'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Settings'));
  return $form;
}

/**
 * Submit handler. Had to make it custom to handle the password logic.
 */
function picasa_albums_admin_options_submit(&$form, $form_state) {

  variable_set('picasa_albums_cache_min', $form_state['values']['picasa_albums_cache_min']);
  variable_set('picasa_albums_thumbnail_size', $form_state['values']['picasa_albums_thumbnail_size']);
  variable_set('picasa_albums_img_size', $form_state['values']['picasa_albums_img_size']);

  if ($form_state['values']['picasa_albums_reset_albums'] == 1) {
    drupal_set_message(t('Updating all albums.'));
    try {
      picasa_albums_update();
    }
    catch (Exception $ex) {
      watchdog('Error occured while updating albums.', $e->getMessage(), array(), WATCHDOG_ERROR);
    }
  }

  // Now clear menu cache since there is logic.
  module_invoke('menu', 'rebuild');

  drupal_set_message(t('Performance settings saved successfully.'));
}

<?php

/**
 * @file
 * Admin page callbacks for the picasa_albums module
 */

/**
 * Presentation page.
 */
function picasa_albums_admin_presentation() {

  if (!$client = _picasa_albums_get_client()) {
    drupal_set_message(t('There was a problem with logging you into Picasa. Please make sure you enter your Google username and password.'), 'error');
  }

  $form['nodetypes'] = array(
    '#type' => 'fieldset',
    '#title' => 'Activate Picasa Albums for following content types',
  );

  $types = array();
  $selected_types = _picasa_albums_get_node_types();
  $result = db_query("SELECT type, name FROM {node_type} ORDER BY name");
  while ($row = db_fetch_object($result)) {
    $types[$row->type] = $row->name;
  }
  $form['nodetypes']['picasa_albums_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node Types'),
    '#description' => 'These are the node types you want to enable Picasa Albums on',
    '#options' => $types,
    '#default_value' => $selected_types,
  );

  $form['captions'] = array(
    '#type' => 'fieldset',
    '#title' => 'Activate / Deactivate captions',
  );
  $options = array(
    0 => t('Hide Image Captions'),
    1 => t('Show Image Captions'),
  );
  $form['captions']['picasa_albums_captions'] = array(
    '#type' => 'radios',
    '#title' => 'Image Caption Settings',
    '#description' => 'This setting exposes the image title to the templates.',
    '#options' => $options,
    '#default_value' => variable_get('picasa_albums_captions', 0),
  );

  $form['viewing'] = array(
    '#type' => 'fieldset',
    '#title' => 'View settings',
  );
  $form['viewing']['picasa_albums_preview_num'] = array(
    '#type' => 'textfield',
    '#title' => 'Images per album for preview',
    '#description' => 'This number of images will display on the node page or teaser with a link to a full album view. Set to 0 for unlimited. For example, if set to 4, only the first 4 images of the album will be displayed on the node, and when you click to view more, you will be taken to a page with all the images in that gallery.',
    '#default_value' => variable_get('picasa_albums_preview_num', 0),
    '#size' => 8,
  );

  $form['viewing']['picasa_albums_view_tab'] = array(
    '#type' => 'checkbox',
    '#title' => 'Expose Albums Tab',
    '#description' => 'This will display an albums tab on the node page which will link to a list of all albums for the specified node.',
    '#default_value' => variable_get('picasa_albums_view_tab', 0),
  );

  $form['viewing']['picasa_albums_view_teaser'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show in teasers',
    '#default_value' => variable_get('picasa_albums_view_teaser', 0),
  );

  $form['viewing']['picasa_albums_view_page'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show in full pages',
    '#default_value' => variable_get('picasa_albums_view_page', 1),
  );

  $form['viewing']['picasa_albums_view_weight'] = array(
    '#type' => 'textfield',
    '#title' => 'Weight',
    '#size' => 5,
    '#default_value' => variable_get('picasa_albums_view_weight', 10),
  );

  $form['viewing']['picasa_albums_node_date'] = array(
    '#type' => 'checkbox',
    '#title' => 'Show date and author on album page',
    '#description' => 'If this is activated, meta information about the album (date and author) are displayed.',
    '#default_value' => variable_get('picasa_albums_node_date', 0),
  );

  // Overview settings.
  $form['overview'] = array(
    '#type' => 'fieldset',
    '#title' => 'Overview settings',
  );

  $form['overview']['picasa_albums_overview_activated'] = array(
    '#type' => 'checkbox',
    '#title' => 'Generate overview page',
    '#description' => 'Overview page is located at http://site/albums and lists all albums.',
    '#default_value' => variable_get('picasa_albums_overview_activated', 0),
  );

  $form['overview']['picasa_albums_overview_links'] = array(
    '#type' => 'checkbox',
    '#title' => 'Link up album page with overview page.',
    '#description' => 'If set, then there is a link appearing on a album page pointing to the album overview page.',
    '#default_value' => variable_get('picasa_albums_overview_links', 0),
  );

  // Templating options.
  $form['templating'] = array(
    '#type' => 'fieldset',
    '#title' => 'Templating',
  );

  $form['templating']['picasa_albums_variable'] = array(
    '#type' => 'checkbox',
    '#title' => 'Theme variable',
    '#description' => 'If activated, then you can access the albums using $node->picasa_albums[\'#albums\'] or $node->picasa_albums[\'#value\'] to embed the pre-generated markup in your theme files.',
    '#default_value' => variable_get('picasa_albums_variable', 0),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Settings'));
  return $form;
}

/**
 * Validate handler.
 */
function picasa_albums_admin_presentation_validate(&$form, $form_state) {

}

/**
 * Submit handler, setting the set variables.
 */
function picasa_albums_admin_presentation_submit(&$form, $form_state) {

  variable_set('picasa_albums_captions', $form_state['values']['picasa_albums_captions']);
  variable_set('picasa_albums_per_node', $form_state['values']['picasa_albums_per_node']);
  variable_set('picasa_albums_node_types', serialize($form_state['values']['picasa_albums_node_types']));
  variable_set('picasa_albums_preview_num', $form_state['values']['picasa_albums_preview_num']);
  variable_set('picasa_albums_view_teaser', $form_state['values']['picasa_albums_view_teaser']);
  variable_set('picasa_albums_view_page', $form_state['values']['picasa_albums_view_page']);
  variable_set('picasa_albums_view_weight', $form_state['values']['picasa_albums_view_weight']);
  variable_set('picasa_albums_view_tab', $form_state['values']['picasa_albums_view_tab']);
  variable_set('picasa_albums_overview_activated', $form_state['values']['picasa_albums_overview_activated']);
  variable_set('picasa_albums_node_date', $form_state['values']['picasa_albums_node_date']);
  variable_set('picasa_albums_overview_links', $form_state['values']['picasa_albums_overview_links']);
  variable_set('picasa_albums_variable', $form_state['values']['picasa_albums_variable']);

  // Now clear menu cache since there is logic.
  module_invoke('menu', 'rebuild');

  drupal_set_message(t('Presentation settings saved successfully.'));
}

<?php

/**
 * @file
 * Create an overview page of the module.
 * Indicates dependencies are successfully installed and listing the options.
 */

/**
 * Main page.
 */
function picasa_albums_admin_main() {

  drupal_set_title(t('Picasa Albums settings'));

  if (variable_get('picasa_albums_google_user', '') == '') {
    drupal_set_message(t('To connect to Picasa you need to enter your Picasa credentials <a href="@path">here</a>.', array('@path' => base_path() . 'admin/settings/picasa_albums/accounts')), 'warning');
  }

  $output = '<h2 class="picasa_albums_admin">Introduction</h2>';

  $output .= '<p><strong>Thanks for using Picasa Albums</strong>. <br />Picasa Albums connects to your Picasa account and makes it possible for you to attach your albums (which can be private or public) to nodes. </p>';

  // Check if ZF2 works.
  try {
    $error_zend = '<span class="ok">OK</span> - ';
    $version = Zend\Version\Version::VERSION;
  }
  catch (Exception $ex) {
    watchdog('Picasa Albums', $ex->getMessage(), array(), WATCHDOG_ERROR);
    $error_zend = '<span class="error">ERROR</span> - ';
  }

  if (module_exists('lightbox2') == FALSE) {
    $error_lightbox2 = '<span class="error">ERROR</span> - ';
  }
  else {
    $error_lightbox2 = '<span class="ok">OK</span> - ';
  }

  $output .= '<h2 class="picasa_albums_admin">' . t('Dependencies') . '</h2>';
  $output .=
  '
  <ol class="picasa_albums_admin">
    <li>' . $error_lightbox2 . '<a href="' . base_path() . 'admin/settings/lightbox2">LightBox2</a></li>
    <li>' . $error_zend . '<a href="' . base_path() . 'admin/reports/status">zend</a> (See Zend Framework to check if properly installed)</li>
  </ol>
  ';

  if (variable_get('picasa_albums_overview_activated', '0') == 0) {
    $overview_active = t('deactivated at the moment, click here to activate.');
  }
  else {
    $overview_active = t('activated at the moment, click here to deactivate.');
  }
  $output .= '<h2 class="picasa_albums_admin">' . t('Quick links') . '</h2>';
  $output .=
  '
  <ol class="picasa_albums_admin">
    <li><a href="' . base_path() . 'albums">' . t('Album overview') . '</a> <a href="' . base_path() . 'admin/settings/picasa_albums/presentation">(' . $overview_active . ')</a></li>
    <li><a href="' . base_path() . 'admin/settings/picasa_albums/accounts">' . t('Picasa Account Settings') . '</a></li>
    <li>node/%/manage-albums ' . t('albums for a given node. (Replace % with a node id)') . '</li>
  </ol>
  ';

  $output .= '<h2 class="picasa_albums_admin">' . t('How to use Picasa Albums') . '</h2>';
  $output .=
  '<ol class="picasa_albums_admin">
  	<li>' . t('Login with your Picasa credentials.') . '</li>
  	<li>' . t('Change the presentation of the albums to your liking.') . '</li>
  	<li>' . t('Modify the <a href="@path">permissions</a>.', array('@path' => base_path() . 'admin/user/permissions#module-picasa_albums')) . '</li>
  	<li>' . t('Tune Picasa Albums to make it perform well by setting the cache duration and image sizes.') . '</li>
  	<li>' . t('Change <a href="#path">LightBox2</a> settings to work nicely with Picasa Albums.', array('@path' => base_path() . 'admin/settings/lightbox2')) . '</li>
  </ol>';

  $output .= '<h2 class="picasa_albums_admin">' . t('Issues') . '</h2>';
  $output .= t('If you find bugs please let me know by creating an issue on the module page. Also, if you have ideas for future features and extensions please feel free to email me.');
  return $output;
}

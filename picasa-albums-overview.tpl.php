<?php

/**
 * @file
 * Picasa albums template file
 * This file shows all albums stored in the database. 
 * For each album the first thumbnail is used as a teaser.
 *
 * $nid is the node these albums belong to.
 * $node_url the url of this page.
 * $albums contains all albums stored in the database.
 */

?>

<?php if (count($albums) == 0): ?>
  <p><?php echo t('No albums found in database.'); ?></p>
<?php else: ?>
  <ul id="picasa_albums_overview">
    <?php foreach ($albums as $album) : ?>
	  <li style="background:url(<?php $images = unserialize(filter_xss($album->images)); echo $images[0]['thumbnail']; ?>);background-repeat:no-repeat">
		<div>
			<a href="node/<?php echo $album->nid; ?>/albums"><?php echo check_plain($album->title); ?></a>
		</div>
	  </li>
    <?php endforeach; ?>
  </ul>
<?php endif;

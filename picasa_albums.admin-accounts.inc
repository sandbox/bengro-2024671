<?php

/**
 * @file
 * Admin page callbacks for the picasa_albums module
 */

/**
 * Picasa account page.
 */
function picasa_albums_admin_accounts() {

  if (!$client = _picasa_albums_get_client()) {
    drupal_set_message(t('There was a problem with logging you into Picasa. Please make sure you enter your Google username and password.'), 'error');
  }

  $form['picasa_account'] = array(
    '#type' => 'fieldset',
    '#title' => t('Picasa Account for API Queries'),
  );
  $form['picasa_account']['picasa_albums_google_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Google Username'),
    '#description' => t('This is the master account which will be used to login to the Google API'),
    '#size' => 20,
    '#default_value' => variable_get('picasa_albums_google_user', ''),
  );
  $form['picasa_account']['picasa_albums_google_pass'] = array(
    '#type' => 'password',
    '#title' => t('Google Password'),
    '#description' => t('If left blank, it will not override the last password set.'),
    '#size' => 20,
    '#default_value' => '',
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Settings'));
  return $form;
}

/**
 * Validate handler.
 */
function picasa_albums_admin_accounts_validate(&$form, $form_state) {

}

/**
 * Submit handler.
 */
function picasa_albums_admin_accounts_submit(&$form, $form_state) {

  variable_set('picasa_albums_google_user', $form_state['values']['picasa_albums_google_user']);
  // Only set the pass if they submitted a value.
  if ($form_state['values']['picasa_albums_google_pass']) {
    variable_set('picasa_albums_google_pass', $form_state['values']['picasa_albums_google_pass']);
  }

  // Now clear menu cache since there is logic.
  module_invoke('menu', 'rebuild');

  drupal_set_message(t('Picasa Albums settings saved successfully.'));
}

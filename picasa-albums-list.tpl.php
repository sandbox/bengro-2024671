<?php
/**
 * @file
 * Picasa Albums template file.
 * This file represents all albums stored at a given node. 
 * This markup is appended to a node.
 *
 * $nid is the node these albums belong to.
 * $albums is an array containing all image data for the node.
 */

// Figure out a clever title.
if (count($albums) == 1):
  // If there is only one album, use Picasa album title.
  $album = reset($albums);
  $title = $album['title'] . ' Album';
elseif (count($albums) > 1):
  // If there are more than two albums, then use node title.
  $referer_node = node_load($nid);
  $title = t('Albums for ') . $referer_node->title;
else:
  $title = t('Albums');
endif;
drupal_set_title($title);
?>

<?php if (count($albums) == 0): ?>
  <p class="picasa_albums_info"><?php t('No albums stored for this node.'); ?></p>
<?php endif; ?>

<?php foreach ($albums as $key => $album): ?>

<div class="album">

  <?php if (variable_get('picasa_albums_node_date', 0) == 1): ?>
  <div class="posted-by"><?php print $album['title']; ?> <?php print $album['postedby']; ?></div>
  <?php endif; ?>

   <?php if (variable_get('picasa_albums_overview_links', 0) == 1): ?>
  <p class="overview-link"><a href="<?php echo base_path() . 'albums'; ?>"><?php echo t('Go to all albums.');?></a></p>
  <?php endif; ?>

  <?php if (count($album['images']) == 0): ?>
    <p><?php echo t('No Images found in this album.'); ?></p>
  <?php elseif (!is_array($album['images'])): ?>
    <p><?php echo t('Parse error, obtained an invalid feed.');?></p>
  <?php else: ?>
    <?php foreach ($album['images'] as $image): ?>
      <a rel="lightbox[<?php print $key; ?>]" href="<?php print $image['image']; ?>"><img src="<?php print $image['thumbnail']; ?>" hspace="2" /></a>
    <?php endforeach; ?>
    <?php if ($album['preview_mode']): ?>
      <?php print $album['link']; ?> (<?php print $album['total_images']; ?>)
    <?php endif; ?>
  <?php endif; ?>

</div>

<?php endforeach;

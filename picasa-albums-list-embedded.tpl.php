<?php

/**
 * @file
 * Picasa albums template file
 *
 * $nid is the node these albums belong to.
 * $albums is an array containing all image data for the node.
 * $album['title'];
 */
?>

<?php foreach ($albums as $key => $album): ?>
<div class="album">
  <?php if (count($album['images']) == 0): ?>
    <p><?php t('No Images found in this album.'); ?></p>
  <?php else: ?>
	  <?php if ($album['preview_mode']): ?>
      <?php print $album['link']; ?>
    <?php endif; ?>

    <?php foreach ($album['images'] as $image): ?>
      <a rel="lightbox[<?php print $key; ?>]" href="<?php print $image['image']; ?>" title="<?php print $image['summary']; ?>"><img src="<?php print $image['thumbnail']; ?>" hspace="2" alt="<?php print $image['summary']; ?>"  /></a>
    <?php endforeach; ?>
  <?php endif; ?>
</div>

<?php endforeach;

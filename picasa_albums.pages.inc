<?php
/**
 * @file
 * Page callbacks for the picasa_albums module
 */

/**
 * Form to add albums to a node.
 */
function picasa_albums_add(&$form_state, $nid) {
  global $user;

  drupal_set_title(t('Manage Albums'));

  $form = array();
  $form['picasa'] = array(
    '#type' => 'fieldset',
    '#title' => t('Picasa Album Management'),
  );

  $remove_any = user_access('remove any albums');
  $remove = $remove_any ? $remove_any : user_access('remove own albums');

  if ($remove_any) {
    $result = db_query("SELECT nid, uid, album_id, title FROM {picasa_albums} WHERE nid = %d", $nid);
  }
  else {
    $result = db_query("SELECT nid, uid, album_id, title FROM {picasa_albums} WHERE nid = %d AND uid = %d", $nid, $user->uid);
  }

  if ($result) {
    $form['picasa']['remove_album']['#tree'] = TRUE;
    while ($row = db_fetch_object($result)) {
      $account = user_load($row->uid);

      if ($remove) {
        $form['picasa']['remove_album'][$row->album_id] = array(
          '#type' => 'checkbox',
          '#title' => t('Remove Album: %title (posted by %user)', array('%title' => $row->title, '%user' => $account->name)),
        );
      }
      else {
        $form['picasa']['remove_album'][$row->album_id] = array(
          '#value' => t('Album: %title (posted by %user)', array('%title' => $row->title, '%user' => $account->name)),
        );
      }
    }
  }

  // Get picasa account.
  $google_user = variable_get('picasa_albums_google_user', '');

  if (!$google_user) {
    $form['picasa']['google_user'] = array(
      '#value' => '<div>' . t('Setup your default Picasa account.', 'user/' . $user->uid . '/edit/picasa-album') . '</div>',
    );
  }

  $limit = variable_get('picasa_albums_captions', 0);
  if ($google_user) {

    $form['picasa']['current_account'] = array(
      '#value' => t('Picasa Account: <b>%gu</b>', array('%gu' => $google_user)),
    );

    $form['picasa']['picasa_album'] = array(
      '#type' => 'textfield',
      '#title' => t('Add Picasa Album'),
      '#description' => t('Start typing the name of the Picasa album here.'),
      '#size' => 30,
      '#autocomplete_path' => 'js/picasa_albums',
    );

    $form['picasa']['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  }

  $form['picasa']['nid'] = array(
    '#type' => 'hidden',
    '#value' => $nid,
  );

  $form['picasa']['uid'] = array(
    '#type' => 'hidden',
    '#value' => $user->uid,
  );

  $form['#validate'][] = 'picasa_albums_validate_handler';
  $form['#submit'][] = 'picasa_albums_submit_handler';

  return $form;
}

/**
 * Submit handler for node form.
 */
function picasa_albums_validate_handler(&$form, $form_state) {
  global $user;

  if (isset($form_state['values']['google_user'])) {
    $google_user = _picasa_albums_get_user_google_account($user->uid);

    if ($google_user == $form_state['values']['google_user']) {
      form_set_error('google_user', t('%user is already the current Picasa account.', array('%user' => $google_user)));
      return;
    }

    $exclude_accounts = variable_get('picasa_albums_exclude_accounts', '');
    $exploded = explode("\n", $exclude_accounts);

    if (count($exploded) > 0 && $exploded[0] != '') {
      $array = array();
      // Create a new array of all lower case values.
      foreach ($exploded as $each) {
        $array[] = drupal_strtolower($each);
      }

      // Compare lower case versions of the strings.
      if (in_array(drupal_strtolower($form_state['values']['google_user']), $array)) {
        form_set_error('google_user', t('You may not use this account.'));
        return;
      }
    }
  }
}

/**
 * Submit handler for node form.
 */
function picasa_albums_submit_handler(&$form, $form_state) {
  global $user;

  // Load up node.
  $node = node_load($form_state['values']['nid']);

  // Process removals.
  if (count($form_state['values']['remove_album']) > 0) {

    foreach ($form_state['values']['remove_album'] as $id => $value) {
      if ($value == 1) {
        db_query("DELETE FROM {picasa_albums} WHERE nid = %d AND album_id = '%s'", $node->nid, $id);
        drupal_set_message(t('Album removed successfully.'));
      }
    }
  }

  if ($form_state['values']['picasa_album']) {

    // Get an array of albums.
    $albums = _picasa_albums_get_albums();

    if ($album_id = array_search($form_state['values']['picasa_album'], $albums)) {
      // Make sure album doesn't already exist.
      $result = db_query("SELECT nid FROM {picasa_albums} WHERE nid = %d AND album_id = %d", $node->nid, $album_id);
      if ($row = db_fetch_object($result)) {
        form_set_error('picasa_album', t('That album already exists in this node.'));
        return;
      }
      // Insert record.
      $google_user = variable_get('picasa_albums_google_user', '');
      $result = db_query("INSERT INTO {picasa_albums} (nid, uid, google_user, album_id, title) VALUES (%d, %d, '%s', '%s', '%s')",
                          $node->nid, $user->uid, $google_user, $album_id, $form_state['values']['picasa_album']);
      drupal_set_message(t('Album added successfully.'));
    }
  }

  if (isset($form_state['values']['google_user'])) {
    picasa_albums_user_edit_submit($form, $form_state);
  }
}

/**
 * Form handler for user/%/edit/picasa.
 */
function picasa_albums_user_edit($form_state, $uid) {
  // Accommodate 'me' module.
  if ($uid == 'me') {
    global $user;
    $uid = $user->uid;
  }

  // Load account.
  $account = user_load($uid);

  $form = array();
  // Load google user if one exists.
  $google_user = _picasa_albums_get_user_google_account($account->uid);

  $form['text'] = array(
    '#value' => t('Setting your Google/Picasa username here will allow you to add albums to content.'),
  );

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => $account->uid,
  );

  $form['google_user'] = _picasa_albums_get_google_user_field();
  $form['google_user']['#default_value'] = $google_user;

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  $form['#validate'][] = 'picasa_albums_validate_handler';

  return $form;
}

/**
 * Implements hook_submit().
 */
function picasa_albums_user_edit_submit(&$form, $form_state) {
  $result = db_query("SELECT google_user FROM {picasa_albums_users} WHERE uid = %d", $form_state['values']['uid']);
  if ($row = db_fetch_object($result)) {
    db_query("UPDATE {picasa_albums_users} SET google_user = '%s' WHERE uid = %d", $form_state['values']['google_user'], $form_state['values']['uid']);
  }
  else {
    db_query("INSERT INTO {picasa_albums_users} (uid, google_user) VALUES (%d, '%s')", $form_state['values']['uid'], $form_state['values']['google_user']);
  }
  drupal_set_message(t('Your Picasa settings have been updated.'));
}

/**
 * Autocomplete callback for get albums.
 */
function picasa_albums_js_albums($string = '') {
  $albums = _picasa_albums_get_albums();

  $matches = array();
  if (is_array($albums)) {
    foreach ($albums as $album) {
      if (stripos($album, $string) !== FALSE) {
        $matches[$album] = check_plain($album);
      }
    }
  }
  drupal_json($matches);
}

/**
 * Page callback for an individual album view.
 */
function picasa_albums_detail($nid, $album_id) {
  if (!$album_id) {
    $albums = _picasa_albums_get_list($nid);
    return theme('picasa_albums_list', $nid, $albums);
  }

  // Load up album.
  $result = db_query("SELECT uid, google_user, album_id, title, added, updated, images FROM {picasa_albums} WHERE nid = %d AND album_id = '%s'", $nid, $album_id);
  $album = array();
  $cache_min = variable_get('picasa_albums_cache_min', 60);

  if ($row = db_fetch_object($result)) {
    $account = user_load($row->uid);
    $album['postedby'] = t('posted by') . ' ' . $account->name;
    $album['author'] = $account->name;
    $album['title'] = $row->title;
    $album['uid'] = $account->uid;

    $cache_expires = $row->updated + $cache_min;
    if (empty($row->images) || time() > $cache_expires) {
      $album['images'] = _picasa_albums_get_images($row->google_user, $row->album_id);
    }
    else {
      $album['images'] = unserialize($row->images);
    }

    $node_url = url('node/' . $nid);
    $title = check_plain($row->title);

    return theme('picasa_albums_detail', $nid, $node_url, $title, $album['uid'], $album['author'], $album['postedby'], $album['images']);
  }
}

/**
 * This creates a page listing all the albums.
 */
function picasa_albums_overview() {
  drupal_set_title(t('Albums Overview'));

  $albums = array();
  $result = db_query("SELECT title, nid, images FROM {picasa_albums}");

  while ($record = db_fetch_object($result)) {
    array_push($albums, $record);
  }

  $node_url = url('node/' . $nid);

  return theme('picasa_albums_overview', $albums);
}
